Atrack

APRS tracker and communicator for mobile devices written in python / elementary.
Targeted especially for the Openmoko Freerunner it utilizes FSO freesmartphone 
middleware framework. Tested and used on SHR.

Atrack can track on different services, i.e. support for youloc.net has been added.

Usage within the aprs network requires amateur radio license (but for non-commercial 
usage you can run your 

own FOSS aprsd server, this is given by the license of the 
APRS protocol, both aprsd server and atrack are FOSS). 

Authors:
Petr Vanek <vanous@penguin.cz>

Artwork:
Most of the icons come from the Tango Icon Library:
http://tango.freedesktop.org/Tango_Icon_Library
The aTrack icons created Petr Vanek under public domain
The Youloc logo comes with permition from FOSS project youloc.net

Homepage:
http://code.google.com/p/atrack/

Links:
SHR
http://shr-project.org/

FSO
http://www.freesmartphone.org/

Elementary
http://trac.enlightenment.org/e/wiki/Elementary

Openmoko
http://wiki.openmoko.org/wiki/Main_Page

Youloc
http://youloc.net/

Aprsd
http://sourceforge.net/projects/aprsd/
