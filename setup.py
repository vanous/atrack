# -*- coding: utf-8 -*-

# aTrack
#
# Copyright (C) 2010 Petr Vanek <vanous@penguin.cz>
# http://code.google.com/p/atrack/
#
# This file is part of aTrack.
#
# aTrack is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# aTrack is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def get_images_list(path):
    l = []
    for root, dirs, files in os.walk(path):
        if root != path: continue
        for name in files:
            if name.endswith('.png'):
                l.append(os.path.join(root, name))
    return l

def main():
    setup(name         = 'atrack',
          version      = '0.0.81',
          description  = 'Embeded linux APRS tool',
          author       = 'Petr Vanek',
          author_email = 'vanous@penguin.cz',
          url          = 'http://code.google.com/p/atrack/',
          classifiers  = [
            'Development Status :: 2 - Beta',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['atrack'],
          scripts      = ['atrack/atrack'],
          data_files   = [
            ('share/applications', ['data/atrack.desktop']),
            ('share/pixmaps', ['data/atrack.png']),
            ('share/atrack/', ['data/atrack.conf']),
            ('share/atrack/images', get_images_list('data/images')),
            ('share/atrack/images/symbols', get_images_list('data/images/symbols')),
            ('share/atrack/images', ['data/images/COPYING']),
            ('share/atrack/images/symbols', ['data/images/symbols/COPYING']),            
            ('share/atrack', ['README']),
            ('share/atrack', ['COPYING']),
            ('share/atrack', ['TODO']),
            ],
          )

if __name__ == '__main__':
    main()
