#!/bin/sh

rm -rf tmp

cd ..
rm -rf build
python setup.py bdist

cd pkg

mkdir tmp
mkdir tmp/CONTROL
cp control tmp/CONTROL

# tar contents into pkg dir
tar -C tmp -xvzf ../dist/atrack*686.tar.gz

fakeroot chown -R root:root tmp

#mv tmp/usr/lib/python2.5/ tmp/usr/lib/python2.6

#ugly hack to force /usr/ instead of /usr/local (python2.6 way)
#when building with bitbake, this is not used and all is cool
mv tmp/usr/local/* tmp/usr/
rm -r tmp/usr/local
# make ipk
./ipkg-build tmp

rm -rf tmp
cd ..
rm -rf build
rm -rf dist